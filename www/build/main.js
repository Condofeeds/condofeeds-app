webpackJsonp([0],{

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_web_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__ = __webpack_require__(123);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, loadingCtrl, oneSignal, network, webservice, nativeStorage, dialogs, device, alertCtrl, app) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.oneSignal = oneSignal;
        this.network = network;
        this.webservice = webservice;
        this.nativeStorage = nativeStorage;
        this.dialogs = dialogs;
        this.device = device;
        this.app = app;
        this.login = {
            // login: '',
            //password: '',
            login: 'sindico@moradadosolfazendinha.com.br',
            password: 'RUBE0568',
            uuid: '',
            serial: '',
            pushToken: '',
            userId: ''
        };
        this.login.uuid = this.device.uuid;
        this.login.serial = this.device.serial;
        this.loading = this.loadingCtrl.create({
            content: "Aguarde....",
        });
        this.alertCtrl = alertCtrl;
        var disconnectSubscription = this.network.onDisconnect().subscribe(function () {
            var alert = _this.alertCtrl.create({
                title: 'Sem conexão! :(',
                subTitle: 'O seu aparelho está sem acesso a internet. Ative a rede de dados ou conecte-se a uma rede Wi-Fi.',
                buttons: ["Ok"]
            });
            alert.present();
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (response) {
            _this.nativeStorage.setItem('OneSignal', {
                pushToken: response.pushToken,
                userId: response.userId,
            }).then(function () {
                _this.login.pushToken = response.pushToken,
                    _this.login.userId = response.userId;
            }, function (error) { return console.error('Error OneSignal item', error); });
        }, function (error) {
            console.log(error);
        });
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.nativeStorage.getItem('User')
            .then(function (data) {
            _this.loading.present();
            _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            _this.loading.dismiss();
        }, function (error) {
            _this.dialogs.alert("Sessão Expirada, Favor Realize seu Login ", 'Atenção', 'Ok');
        });
    };
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        this.loading.present();
        this.oneSignal.getIds().then(function (response) {
            _this.nativeStorage.setItem('OneSignal', {
                pushToken: response.pushToken,
                userId: response.userId,
            }).then(function () {
                console.log(response);
                _this.login.pushToken = response.pushToken,
                    _this.login.userId = response.userId;
            }, function (error) { return console.error('Error OneSignal item', error); });
        }, function (error) {
            console.log(error);
        });
        this.webservice.getLogin(this.login).subscribe(function (response) {
            if (response.status) {
                if (_this.device.uuid != null) {
                    _this.nativeStorage.setItem('User', {
                        uuid: _this.device.uuid,
                        serial: _this.device.serial,
                        cpf_cnpj: response.obj.cpf_cnpj,
                        nome: response.obj.nome,
                        login: response.obj.login,
                        tipo: response.obj.tipo_usuario,
                        token: response.obj.token,
                        condominio: response.obj.condominio,
                        nome_condominio: response.obj.nome_condominio,
                        logo_condominio: response.obj.logo_condominio,
                        bloco: response.obj.bloco,
                        id_apto: response.obj.id_apto,
                        num_apto: response.obj.num_apto
                    })
                        .then(function () { return console.log('Stored User!'); }, function (error) { return console.error('Error storing item', error); });
                }
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                _this.loading.dismiss();
            }
            else {
                _this.loading.dismiss();
                _this.dialogs.alert(response.msg, 'Atenção', 'ok');
            }
        });
    };
    LoginPage.prototype.forgotPassword = function () {
        var alert = this.alertCtrl.create({
            title: ' Esqueceu a senha ?!',
            subTitle: ' Sua senha foi enviada para o seu e-mail de cadastro!',
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\login\login.html"*/'<ion-content>\n\n  <ion-row>\n\n    <ion-col>\n\n      <img class="img" src="assets/img/logo_1.png" />\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-col></ion-col>\n\n  <div>\n\n    <form class="form" novalidate>\n\n      <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col col-9>\n\n          <ion-item>\n\n            <ion-label stacked>E-mail</ion-label>\n\n            <ion-input type="email" name="email" [(ngModel)]="login.login" required></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col col-9>\n\n          <ion-item>\n\n            <ion-label stacked>Senha</ion-label>\n\n            <ion-input type="password" name="password" [(ngModel)]="login.password" required></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n\n    <ion-row>\n\n      <br>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-1></ion-col>\n\n      <ion-col col-5>\n\n        <button class="esqsenha" item-right clear ion-button block (click)="forgotPassword()">Esqueceu a senha ?</button>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <button item-right ion-button block (click)="doLogin()">Entrar</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_3__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsReservationPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_web_service__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DetailsReservationPage = (function () {
    function DetailsReservationPage(navCtrl, navParams, modalCtrl, webservice, loadingCtrl, nativeStorage, dialogs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.webservice = webservice;
        this.loadingCtrl = loadingCtrl;
        this.nativeStorage = nativeStorage;
        this.dialogs = dialogs;
        this.detailList = {
            nome: "",
            descricao: null,
            valor: "",
            limite_pessoas: "",
            limite_horario: "",
            id_area: ""
        };
        this.tokenListEvent = {
            token: "",
            cnpj_condominio: "",
            id_area_comun: "",
            aprovacao: "A",
            week: "5"
        };
        this.loading = this.loadingCtrl.create({
            content: "<ion-spinner name=\"crescent\"></ion-spinner>",
        });
    }
    DetailsReservationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.detailList = this.navParams.get('details');
        /*this.tokenListEvent.id_area_comun = this.detailList.id_area;
          this.webservice.getListEvent(this.tokenListEvent).then(response => {
            if (response.status) {
              this.getListEvent = response.obj;
            } else {
              this.eventList = true;
            }
          }, error => {
            console.log(error);
            Dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
          });  */
        this.nativeStorage.getItem('User').then(function (response) {
            _this.tokenListEvent.token = response.token;
            _this.tokenListEvent.cnpj_condominio = response.condominio;
            _this.tokenListEvent.id_area_comun = _this.detailList.id_area;
            _this.webservice.getListEvent(_this.tokenListEvent).subscribe(function (response) {
                if (response.status) {
                    _this.loading.present();
                    setTimeout(function () {
                        _this.loading.dismiss();
                        _this.getListEvent = response.obj;
                    }, 300);
                }
                else {
                    _this.loading.present();
                    setTimeout(function () {
                        _this.loading.dismiss();
                        _this.eventList = true;
                    }, 300);
                }
            }, function (error) {
                _this.dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
            });
        }, function (error) {
            alert('error');
        });
    };
    DetailsReservationPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(ModalPage);
        modal.present();
    };
    DetailsReservationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details-reservation',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\details-reservation\details-reservation.html"*/'<ion-content>\n\n  <ion-card>\n\n    <ion-card-header>\n\n      <h1>{{detailList.nome}}</h1>\n\n      <hr>\n\n      <h3 class="descricao">\n\n        <b>DESCRIÇÃO</b>\n\n      </h3>\n\n    </ion-card-header>\n\n    <ion-item text-wrap>\n\n      <p>{{detailList.descricao}}</p>\n\n      <hr>\n\n    </ion-item>\n\n    <ion-item>\n\n      <p item-left>\n\n        <ion-icon name="card"></ion-icon>\n\n      </p>\n\n      <p item-left>Valor: {{detailList.valor}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <p item-left>\n\n        <ion-icon name="body"></ion-icon>\n\n      </p>\n\n      <p item-left>Limite de Pessoas: {{detailList.limite_pessoas}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <p item-left>\n\n        <ion-icon name="clock"></ion-icon>\n\n      </p>\n\n      <p item-left> Limite de Horário: {{detailList.limite_horario}}</p>\n\n    </ion-item>\n\n  </ion-card>\n\n  <h5 class="title">PRÓXIMOS EVENTOS</h5>\n\n  <p class="event" *ngIf="eventList">Não há eventos</p>\n\n  <ion-item *ngFor="let listEvents of getListEvent">\n\n    <ion-thumbnail item-left>\n\n      <h2 class=\'data_day\'>\n\n        <b>{{listEvents.dia}}</b>\n\n      </h2>\n\n      <p class=\'data_month\'>\n\n        <b>{{listEvents.mes}}</b>\n\n      </p>\n\n    </ion-thumbnail>\n\n    <h2 class="apto_bloco">\n\n      <b>{{listEvents.descricao}}</b>\n\n    </h2>\n\n    <p class="apto_bloco">Bloco: {{listEvents.bloco}} | Apto: {{listEvents.apto}}</p>\n\n  </ion-item>\n\n  <ion-fab right bottom>\n\n    <button ion-fab mini (click)="openModal()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\details-reservation\details-reservation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__["a" /* Dialogs */]])
    ], DetailsReservationPage);
    return DetailsReservationPage;
}());

var ModalPage = (function () {
    function ModalPage(params, viewCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.day = new Date().getUTCDate();
        this.month = new Date().getMonth() + 1;
        this.year = new Date().getFullYear();
        if (this.month < 10) {
            this.month = "0" + this.month;
        }
        if (this.day < 10) {
            this.day = "0" + this.day;
        }
        else {
            this.month = this.month;
        }
        this.min = this.year + "-" + this.month + "-" + this.day;
    }
    ModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            template: "<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Reserva\n    </ion-title>\n    <ion-buttons start>\n      <button ion-button (click)=\"dismiss()\">\n        <ion-icon name=\"md-close\" ></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n<ion-card>\n  <ion-item class=\"top\">\n    <ion-avatar item-left>\n      <img src=\"img/marty-avatar.png\">\n    </ion-avatar>\n    <h2>Welton Lima</h2>\n    <p>Apartamento 1004 - Bloco 6 </p>\n  </ion-item>\n  <ion-item class=\"section-1\">\n     <p item-left>Selecione uma data para a reserva</p>\n  </ion-item>    \n    <ion-item > \n    <span item-left>\n        <ion-icon ion-fab mini name=\"calendar\"></ion-icon>\n    </span>\n     <span item-left>\n        <ion-datetime cancelText=\"Cancelar\" doneText=\"Aceitar\" displayFormat=\"DD MMMM YYYY\" min=\"{{min}}\" max=\"{{year}}\" ></ion-datetime>\n    </span> \n  </ion-item>\n      <ion-item class=\"footer\">\n        <button ion-button icon-left clear item-right>\n         CONFIRMAR\n    </button>\n  </ion-item>\n</ion-card>"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=details-reservation.js.map

/***/ }),

/***/ 142:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 142;

/***/ }),

/***/ 185:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 185;

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_web_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__orders_orders__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__reservation_reservation__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__feeds_feeds__ = __webpack_require__(233);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navCtrl, webservice, app, nativeStorage) {
        this.navCtrl = navCtrl;
        this.webservice = webservice;
        this.app = app;
        this.nativeStorage = nativeStorage;
        this.tokenCondominio = {
            token: '',
            condominio: ''
        };
        this.userData = {
            nome: '',
            bloco: '',
            num_apto: '',
            avatar: 'https://www.condofeeds.com.br/condofeeds/assets/images/user_padrao.jpg'
        };
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.nativeStorage.getItem('User')
            .then(function (data) {
            _this.userData.nome = data.nome;
            _this.userData.bloco = data.bloco;
            _this.userData.num_apto = data.num_apto;
            _this.tipoUsuario = data.tipo;
        }, function (error) { return console.log(error); });
    };
    HomePage.prototype.exit = function () {
        if (this.nativeStorage.remove('User')) {
            this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
        }
        else {
            console.log("Error");
        }
    };
    HomePage.prototype.order = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_5__orders_orders__["a" /* OrdersPage */]);
    };
    HomePage.prototype.ListFeeds = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_7__feeds_feeds__["a" /* FeedsPage */]);
    };
    HomePage.prototype.Reservation = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_6__reservation_reservation__["a" /* ReservationPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title class="ion-title">Morada do Sol Fazendinha</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="list-avatar-page icons-basic-page grid-basic-page">\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-avatar item-left>\n\n        <img src="{{userData.avatar}}">\n\n      </ion-avatar>\n\n      <h2>{{userData.nome}}</h2>\n\n      <p *ngIf="tipoUsuario == \'#sindico\'">Perfil: {{tipoUsuario}} </p>\n\n      <p *ngIf="tipoUsuario == \'#residente\'">Bloco: {{userData.bloco}} Apto: {{userData.num_apto}} </p>\n\n    </ion-item>\n\n  </ion-list>\n\n  <div class="menu">\n\n    <ion-grid>\n\n      <ion-row>\n\n        <ion-col col-8>\n\n          <div class="item-1 menu-item-1" (click)="order()">\n\n            <ion-icon name="cube"></ion-icon>\n\n            <br>\n\n            <br> Portaria\n\n            <br>\n\n            <br>\n\n            <br>\n\n            <br>\n\n            <br>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <div class="item-2 menu-item-2" (click)="ListFeeds()">\n\n            <ion-icon name="paper"> </ion-icon>\n\n            <br>\n\n            <br> Comunicados\n\n            <br>\n\n            <br>\n\n          </div>\n\n          <div class="item-3 menu-item-3" (click)="exit()">\n\n            <ion-icon name="exit"></ion-icon>\n\n            <br>\n\n            <br> Logout\n\n            <br>\n\n            <br>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-12>\n\n          <div class="item-4 menu-item-4" (click)="Reservation()">\n\n            <ion-icon name="calendar"> </ion-icon>\n\n            <br>\n\n            <br> Reservas\n\n            <br>\n\n            <br>\n\n            <br>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_web_service__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OrdersPage = (function () {
    function OrdersPage(navCtrl, navParams, webService, loadingCtrl, nativeStorage, dialogs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.webService = webService;
        this.loadingCtrl = loadingCtrl;
        this.nativeStorage = nativeStorage;
        this.dialogs = dialogs;
        this.encomendas = "encomendas";
        this.isAndroid = false;
        this.base_url = "https://www.condofeeds.com.br/condofeeds/";
        this.getOrders = {
            token: '',
            condominio: '',
            id_apto: ''
        };
        this.loading = this.loadingCtrl.create({
            content: "Aguarde....",
        });
    }
    OrdersPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.loading.present();
        this.nativeStorage.getItem('User')
            .then(function (data) {
            _this.getOrders.token = data.token;
            _this.getOrders.condominio = data.condominio;
            _this.getOrders.id_apto = data.id_apto;
            _this.webService.getOrders(_this.getOrders).subscribe(function (response) {
                if (response.status) {
                    _this.listOrders = response.obj;
                    _this.visitas = response.visitantes;
                    _this.loading.dismiss();
                }
                else {
                    _this.loading.dismiss();
                    _this.dialogs.alert(response.msg, 'Atenção', 'Ok');
                }
            }, function (error) {
                _this.loading.dismiss();
                _this.dialogs.alert("Não possível buscar Encomendas ", 'Atenção', 'Ok');
            });
        }, function (error) {
            _this.getOrders.token = "32838a4e42d0c8729910f3a67b590dd7e0bf99d485cdadc0c3e8cce0b7e4ebf170928e29a4a4b88c78d9399c453f1769759a9c65a3ca5dfb18a76a97c41919aa67697363686566666572616c76657340686f746d61696c2e636f6d";
            _this.getOrders.condominio = "05488161000190";
            _this.getOrders.id_apto = "396";
            _this.webService.getOrders(_this.getOrders).subscribe(function (response) {
                _this.loading.dismiss();
                _this.listOrders = response.obj;
            });
        });
    };
    OrdersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-orders',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\orders\orders.html"*/'<ion-header>\n\n  <ion-navbar hide-nav-bar="true">\n\n    <ion-title>Portaria</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div padding>\n\n    <ion-segment [(ngModel)]="encomendas">\n\n      <ion-segment-button value="encomendas">\n\n        Encomendas\n\n      </ion-segment-button>\n\n      <ion-segment-button value="visitas">\n\n        Visitas\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n  </div>\n\n  <div [ngSwitch]="encomendas">\n\n    <ion-list *ngSwitchCase="\'encomendas\'">\n\n      <ion-item *ngFor="let item of listOrders">\n\n        <h4>{{item.descricao}}</h4>\n\n        <p>Recebido por: {{item.nome}}</p>\n\n        <ion-note item-right>{{item.data}} {{item.hora}}</ion-note>\n\n      </ion-item>\n\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'visitas\'">\n\n      <ion-item *ngFor="let visita of visitas">\n\n        <ion-thumbnail item-left>\n\n          <img src="{{base_url}}{{visita.img}}" imageViewer />\n\n        </ion-thumbnail>\n\n        <h2>{{visita.nome}}</h2>\n\n        <p>Motivo: {{visita.observacao}}</p>\n\n        <ion-note item-right>{{visita.data}}</ion-note>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\orders\orders.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */]])
    ], OrdersPage);
    return OrdersPage;
}());

//# sourceMappingURL=orders.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__details_reservation_details_reservation__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_web_service__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ReservationPage = (function () {
    function ReservationPage(navCtrl, navParams, webservice, loadingCtrl, dialogs, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.webservice = webservice;
        this.loadingCtrl = loadingCtrl;
        this.dialogs = dialogs;
        this.nativeStorage = nativeStorage;
        this.tokenList = {
            token: "",
            cnpj_condominio: "",
            id_area: "null"
        };
        this.loading = this.loadingCtrl.create({
            content: "Aguarde....",
        });
    }
    ReservationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loading.present();
        this.nativeStorage.getItem('User').then(function (response) {
            _this.tokenList.token = response.token;
            _this.tokenList.cnpj_condominio = response.condominio;
            _this.webservice.getList(_this.tokenList).subscribe(function (response) {
                if (response.status) {
                    _this.loading.dismiss();
                    _this.getList = response.obj;
                }
                else {
                    _this.loading.dismiss();
                    _this.dialogs.alert(response.msg, 'Atenção', 'Ok');
                }
            }, function (error) {
                _this.loading.dismiss();
                _this.dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
            });
        }, function (error) {
            _this.loading.dismiss();
            _this.dialogs.alert("Não possível buscar dados do usuário.", 'Atenção', 'Ok');
        });
    };
    ReservationPage.prototype.DetailsReservation = function (details) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__details_reservation_details_reservation__["a" /* DetailsReservationPage */], { details: details });
    };
    ReservationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reservation',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\reservation\reservation.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Reservas</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-item (click)="DetailsReservation(list)" *ngFor="let list of getList">\n\n      <ion-thumbnail item-left>\n\n        <img src="assets/img/image-not-found-medium.gif">\n\n      </ion-thumbnail>\n\n      <h2>{{list.nome}}</h2>\n\n      <p>Limite de Pessoas: {{list.limite_pessoas}}</p>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\reservation\reservation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], ReservationPage);
    return ReservationPage;
}());

//# sourceMappingURL=reservation.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__detail_feeds_detail_feeds__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_web_service__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FeedsPage = (function () {
    function FeedsPage(navCtrl, navParams, webservice, nativeStorage, dialogs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.webservice = webservice;
        this.nativeStorage = nativeStorage;
        this.dialogs = dialogs;
        this.token = {
            token: "",
            condominio: ""
        };
        this.isRefreshing = false;
    }
    FeedsPage.prototype.doRefresh = function (refresher) {
        this.refresher = refresher;
        this.isRefreshing = true;
        this.loadFeeds();
    };
    FeedsPage.prototype.ionViewDidEnter = function () {
        this.loadFeeds();
    };
    FeedsPage.prototype.loadFeeds = function () {
        var _this = this;
        this.nativeStorage.getItem('User').then(function (response) {
            _this.token.token = response.token;
            _this.token.condominio = response.condominio;
            _this.webservice.getFeeds(_this.token).subscribe(function (response) {
                if (response.status) {
                    _this.listFeeds = response.obj;
                    if (_this.isRefreshing) {
                        _this.refresher.complete();
                        _this.isRefreshing = false;
                    }
                }
                else {
                    _this.dialogs.alert(response.msg, 'Atenção', 'Ok');
                    if (_this.isRefreshing) {
                        _this.refresher.complete();
                        _this.isRefreshing = false;
                    }
                }
            }, function (error) {
                _this.dialogs.alert("Não possível buscar Comunicados.", 'Atenção', 'Ok');
                if (_this.isRefreshing) {
                    _this.refresher.complete();
                    _this.isRefreshing = false;
                }
            });
        }, function (error) {
            _this.dialogs.alert("Não possível buscar dados do usuário.", 'Atenção', 'Ok');
            if (_this.isRefreshing) {
                _this.refresher.complete();
                _this.isRefreshing = false;
            }
        });
    };
    FeedsPage.prototype.openFeeds = function (details) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__detail_feeds_detail_feeds__["a" /* DetailFeedsPage */], { details: details });
    };
    FeedsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-feeds',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\feeds\feeds.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title class="ion-title">Comunicados</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <feed endIcon="call">\n\n    <feed-item *ngFor="let item of listFeeds">\n\n      <feed-tittle [tittle]="item.descricao"></feed-tittle>\n\n      <ion-card>\n\n        <ion-card-content>\n\n          <p [innerHTML]="item.feed"></p>\n\n        </ion-card-content>\n\n        <ion-row>\n\n          <ion-col center text-center>\n\n            <ion-note>\n\n\n\n            </ion-note>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button icon-left clear small>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col center text-center>\n\n                <ion-note (click)=\'openFeeds(item)\'>\n\n                    (mais...)\n\n                  </ion-note>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col center text-center>\n\n            <ion-note>\n\n              {{item.dateDiff}}\n\n            </ion-note>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button icon-left clear small>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col center text-center>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </feed-item>\n\n  </feed>\n\n\n\n\n\n\n\n  <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <ion-list-header>Comunicados</ion-list-header>\n\n    <ion-item *ngFor="let item of listFeeds" (click)="openFeeds(item)">\n\n      <h2>{{item.descricao}}</h2>\n\n      <p [innerHTML]="item.feed"></p>\n\n      <ion-icon name="arrow-round-forward" item-right></ion-icon>\n\n      <p>\n\n        <ion-note item-right> {{item.dateDiff}} </ion-note>\n\n      </p>\n\n    </ion-item>\n\n  </ion-list> -->\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\feeds\feeds.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_web_service__["a" /* WebServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */]])
    ], FeedsPage);
    return FeedsPage;
}());

//# sourceMappingURL=feeds.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailFeedsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailFeedsPage = (function () {
    function DetailFeedsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.detail = { feed: "", descricao: "", caminho: "", arquivo: "" };
    }
    DetailFeedsPage.prototype.ionViewDidLoad = function () {
        this.detail = this.navParams.get('details');
    };
    DetailFeedsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detail-feeds',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\detail-feeds\detail-feeds.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Comunicados</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card>\n\n    <ion-card-header>\n\n      <p>{{detail.descricao}}</p>\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <p [innerHTML]="detail.feed"></p>\n\n    </ion-card-content>\n\n\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\pages\detail-feeds\detail-feeds.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */]])
    ], DetailFeedsPage);
    return DetailFeedsPage;
}());

//# sourceMappingURL=detail-feeds.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(267);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_orders_orders__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_feeds_feeds__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_detail_feeds_detail_feeds__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_reservation_reservation__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_details_reservation_details_reservation__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_web_service__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_device__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_dialogs__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_onesignal__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_network__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ionic_img_viewer__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_feed_feed__ = __webpack_require__(419);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_feeds_feeds__["a" /* FeedsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_detail_feeds_detail_feeds__["a" /* DetailFeedsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_reservation_reservation__["a" /* ReservationPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_details_reservation_details_reservation__["a" /* DetailsReservationPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_details_reservation_details_reservation__["b" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_21__components_feed_feed__["a" /* FeedComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_feed_feed__["b" /* FeedItemComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_feed_feed__["c" /* FeedTittleComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_20_ionic_img_viewer__["a" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
                }, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_feeds_feeds__["a" /* FeedsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_detail_feeds_detail_feeds__["a" /* DetailFeedsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_reservation_reservation__["a" /* ReservationPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_details_reservation_details_reservation__["a" /* DetailsReservationPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_details_reservation_details_reservation__["b" /* ModalPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_storage__["a" /* NativeStorage */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_dialogs__["a" /* Dialogs */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_onesignal__["a" /* OneSignal */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_network__["a" /* Network */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicErrorHandler */], },
                __WEBPACK_IMPORTED_MODULE_14__providers_web_service__["a" /* WebServiceProvider */],
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(124);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, onesignal, nativeStorage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.onesignal = onesignal;
        this.nativeStorage = nativeStorage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            _this.onesignal.startInit('57322a76-745c-4100-8bf3-4d1eef110a94', '837667551466');
            _this.onesignal.inFocusDisplaying(_this.onesignal.OSInFocusDisplayOption.InAppAlert);
            _this.onesignal.handleNotificationReceived().subscribe(function () { });
            _this.onesignal.handleNotificationOpened().subscribe(function () { });
            _this.onesignal.endInit();
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FeedItemComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return FeedTittleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the FeedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FeedComponent = (function () {
    function FeedComponent() {
        this.endIcon = 'ionic';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('endIcon'),
        __metadata("design:type", Object)
    ], FeedComponent.prototype, "endIcon", void 0);
    FeedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'feed',template:/*ion-inline-start:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\components\feed\feed.html"*/'<div class="feed">\n\n  <ng-content></ng-content>\n\n\n\n  <feed-item>\n\n      <ion-icon class="" [name]="endIcon"></ion-icon>\n\n  </feed-item>\n\n\n\n</div>\n\n'/*ion-inline-end:"C:\Users\wlads\Documents\condofeeds\condofeeds-app\src\components\feed\feed.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], FeedComponent);
    return FeedComponent;
}());

var FeedItemComponent = (function () {
    function FeedItemComponent() {
    }
    FeedItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'feed-item',
            template: '<ng-content></ng-content>'
        }),
        __metadata("design:paramtypes", [])
    ], FeedItemComponent);
    return FeedItemComponent;
}());

var FeedTittleComponent = (function () {
    function FeedTittleComponent() {
        this.titulo = '';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tittle'),
        __metadata("design:type", String)
    ], FeedTittleComponent.prototype, "titulo", void 0);
    FeedTittleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'feed-tittle',
            template: '<span>{{titulo}}</span>'
        }),
        __metadata("design:paramtypes", [])
    ], FeedTittleComponent);
    return FeedTittleComponent;
}());

//# sourceMappingURL=feed.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WebServiceProvider = (function () {
    function WebServiceProvider(http) {
        this.http = http;
        this.url = 'https://www.condofeeds.com.br/condofeeds/restserver/';
        this.version = '01/';
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Accept': 'application/json',
            'Authorization': 'Basic YXBpOkt6clRNU0QkOTAwTnUxamhRNE0heU9tdzM0MmZZRDYjJkhXbWwkVU1INjEqeXlEMzFkRjRlUCFtdG0xMVdsRE8='
        });
        console.log('Hello WebServiceProvider Provider');
    }
    //: Observable<JSON>
    WebServiceProvider.prototype.getLogin = function (login) {
        return this.http.post(this.url + this.version + 'auth/authentication', login, { headers: this.headers }).map(function (res) { return res.json(); });
    };
    WebServiceProvider.prototype.getFeeds = function (tokenCondominio) {
        return this.http.post(this.url + this.version + 'Feed', tokenCondominio, { headers: this.headers }).map(function (res) { return res.json(); });
    };
    WebServiceProvider.prototype.getOrders = function (getOrders) {
        return this.http.post(this.url + this.version + 'orders', getOrders, { headers: this.headers }).map(function (res) { return res.json(); });
    };
    WebServiceProvider.prototype.getList = function (getList) {
        return this.http.post(this.url + this.version + 'AreaComum/lista', getList, { headers: this.headers }).map(function (res) { return res.json(); });
    };
    WebServiceProvider.prototype.getListEvent = function (getListEvent) {
        return this.http.post(this.url + this.version + 'AreaComum/listReservas', getListEvent, { headers: this.headers }).map(function (res) { return res.json(); });
    };
    WebServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], WebServiceProvider);
    return WebServiceProvider;
}());

//# sourceMappingURL=web-service.js.map

/***/ })

},[258]);
//# sourceMappingURL=main.js.map