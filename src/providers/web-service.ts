import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Login } from '../interface/login';
import { TokenCondominio } from '../interface/tokenCondominio';
import { GetOrders } from '../interface/orders/getOrders';
import { List } from '../interface/reservation/list';
import { ListReservation } from '../interface/reservation/listReservation';
import { TokenList } from '../interface/reservation/tokenList';
import { TokenListEvent } from '../interface/reservation/tokenListEvent';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WebServiceProvider {

    private url: string = 'https://www.condofeeds.com.br/condofeeds/restserver/';
    private version: string = '01/';

    private headers = new Headers(
        {
            'Accept': 'application/json',
            'Authorization': 'Basic YXBpOkt6clRNU0QkOTAwTnUxamhRNE0heU9tdzM0MmZZRDYjJkhXbWwkVU1INjEqeXlEMzFkRjRlUCFtdG0xMVdsRE8='
        }
    );

    constructor(public http: Http) {
        console.log('Hello WebServiceProvider Provider');
    }

    //: Observable<JSON>

    getLogin(login: Login) {
        return this.http.post(this.url + this.version + 'auth/authentication', login,{headers:this.headers}).map(res => res.json());
    }

    getFeeds(tokenCondominio: TokenCondominio) {
        return this.http.post(this.url + this.version + 'Feed', tokenCondominio, { headers: this.headers }).map(res => res.json());
    }

    getOrders(getOrders: GetOrders) {
        return this.http.post(this.url + this.version + 'orders', getOrders, { headers: this.headers }).map(res => res.json())
    }

    getList(getList: TokenList) {
        return this.http.post(this.url + this.version + 'AreaComum/lista', getList, { headers: this.headers }).map(res => res.json());
    }

    getListEvent(getListEvent: TokenListEvent) {
        return this.http.post(this.url + this.version + 'AreaComum/listReservas', getListEvent, { headers: this.headers }).map(res => res.json());
    }


}
