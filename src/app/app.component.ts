import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { OneSignal } from '@ionic-native/onesignal';
import { NativeStorage } from '@ionic-native/native-storage';

import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LoginPage;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public onesignal: OneSignal,
    public nativeStorage: NativeStorage
  ) {
    platform.ready().then(() => {
      this.onesignal.startInit('57322a76-745c-4100-8bf3-4d1eef110a94', '837667551466');
      this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.InAppAlert);
      this.onesignal.handleNotificationReceived().subscribe(() => { });
      this.onesignal.handleNotificationOpened().subscribe(() => { });
      this.onesignal.endInit();

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
