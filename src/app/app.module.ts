import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { OrdersPage } from '../pages/orders/orders';
import { FeedsPage } from '../pages/feeds/feeds';
import { TimelineFeedsPage } from '../pages/timeline-feeds/timeline-feeds';
import { DetailFeedsPage } from '../pages/detail-feeds/detail-feeds';
import { ReservationPage } from '../pages/reservation/reservation';
import { DetailsReservationPage } from '../pages/details-reservation/details-reservation';
import { ModalPage } from '../pages/details-reservation/details-reservation';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { WebServiceProvider } from '../providers/web-service';

import { Device } from '@ionic-native/device';
import { NativeStorage } from '@ionic-native/native-storage';
import { Dialogs } from '@ionic-native/dialogs';
import { OneSignal } from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FeedComponent, FeedItemComponent, FeedTittleComponent } from '../components/feed/feed';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    OrdersPage,
    FeedsPage,
    DetailFeedsPage,
    ReservationPage,
    DetailsReservationPage,
    ModalPage,
    FeedComponent,
    FeedItemComponent,
    FeedTittleComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicImageViewerModule,
    IonicModule.forRoot(MyApp, {
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    OrdersPage,
    FeedsPage,
    DetailFeedsPage,
    ReservationPage,
    DetailsReservationPage,
    ModalPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    NativeStorage,
    Dialogs,
    OneSignal,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler, },
    WebServiceProvider,
  ],

})
export class AppModule { }
