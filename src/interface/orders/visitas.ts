export interface Visitas {
    cpf_cnpj: string,
    nome: string,
    data: string,
    img: string,
    observacao: string
}