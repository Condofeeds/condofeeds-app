export interface DetailList {
      nome: String,
      descricao: null,
      valor: String,
      limite_pessoas: String,
      limite_horario: String,
      id_area: String
}