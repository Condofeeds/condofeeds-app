export interface TokenListEvent {
      token: String,
      cnpj_condominio: String,
      id_area_comun: String,
      aprovacao: String,
      week: String
}