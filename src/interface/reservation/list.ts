export interface List {
      id_area: String,
      nome: String,
      descricao: null,
      valor: String,
      cnpj_condominio: String,
      login_usuario: String,
      detalhes: String,
      dias_antecedencia: String,
      limite_pessoas: String,
      limite_horario: String,
      bloqueio: String,
      caminho: String,
      arquivo: String
}