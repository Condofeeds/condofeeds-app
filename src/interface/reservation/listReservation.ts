export interface ListReservation {
     id_reserva: String,
      id_area_comun: String,
      id_calendario: String,
      id_apartamento: String,
      cnpj_condominio: String,
      data_inicio_reserva: String,
      dia: String,
      mes: String,
      local: String,
      descricao: String,
      aprovacao: String,
      apto: String,
      bloco: String
}