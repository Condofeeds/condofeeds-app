import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Dialogs } from '@ionic-native/dialogs';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController } from 'ionic-angular';

import { GetOrders } from '../../interface/orders/getOrders';
import { ListOrders } from '../../interface/orders/listOrders';
import { Visitas } from '../../interface/orders/Visitas';

import { WebServiceProvider } from '../../providers/web-service';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html'
})
export class OrdersPage {
  public encomendas: string = "encomendas";
  public isAndroid: boolean = false;
  public base_url = "https://www.condofeeds.com.br/condofeeds/";
  public loading: any;
  public getOrders: GetOrders = {
    token: '',
    condominio: '',
    id_apto: ''
  };
  public listOrders: ListOrders[];
  public visitas: Visitas[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public webService: WebServiceProvider,
    public loadingCtrl: LoadingController,
    public nativeStorage: NativeStorage,
    public dialogs: Dialogs
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Aguarde....",
    });
  }

  ionViewDidEnter() {
    this.loading.present();
    this.nativeStorage.getItem('User')
      .then(data => {
        this.getOrders.token = data.token;
        this.getOrders.condominio = data.condominio;
        this.getOrders.id_apto = data.id_apto;

        this.webService.getOrders(this.getOrders).subscribe(response => {
          if (response.status) {
            this.listOrders = response.obj;
            this.visitas = response.visitantes;
            this.loading.dismiss();
          } else {
            this.loading.dismiss();
            this.dialogs.alert(response.msg, 'Atenção', 'Ok');
          }
        }, error => {
          this.loading.dismiss();
          this.dialogs.alert("Não possível buscar Encomendas ", 'Atenção', 'Ok');
        });
      },
      error => {
        this.getOrders.token = "32838a4e42d0c8729910f3a67b590dd7e0bf99d485cdadc0c3e8cce0b7e4ebf170928e29a4a4b88c78d9399c453f1769759a9c65a3ca5dfb18a76a97c41919aa67697363686566666572616c76657340686f746d61696c2e636f6d";
        this.getOrders.condominio = "05488161000190";
        this.getOrders.id_apto = "396";

        this.webService.getOrders(this.getOrders).subscribe(response => {
          this.loading.dismiss();
          this.listOrders = response.obj;
        });
      })
  }
}
