import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DetailFeeds } from '../../interface/detailsFeeds';

@Component({
  selector: 'page-detail-feeds',
  templateUrl: 'detail-feeds.html'
})
export class DetailFeedsPage {

  public detail: DetailFeeds = { feed: "", descricao: "", caminho: "", arquivo: "" };

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    this.detail = this.navParams.get('details');
  }
}
