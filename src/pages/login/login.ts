import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { Login } from '../../interface/login';
import { HomePage } from '../home/home';

import { WebServiceProvider } from '../../providers/web-service';
import { Dialogs } from '@ionic-native/dialogs';
import { NativeStorage } from '@ionic-native/native-storage';
import { Device } from '@ionic-native/device';

import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    alertCtrl: AlertController;
    public loading: any;
    public login: Login = {
       // login: '',
        //password: '',
        login: 'sindico@moradadosolfazendinha.com.br',
        password: 'RUBE0568',
        uuid: '',
        serial: '',
        pushToken: '',
        userId: ''
    };

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public oneSignal: OneSignal,
        public network: Network,
        public webservice: WebServiceProvider,
        public nativeStorage: NativeStorage,
        public dialogs: Dialogs,
        public device: Device,
        alertCtrl: AlertController,
        public app: App

    ) {
        this.login.uuid = this.device.uuid;
        this.login.serial = this.device.serial;
        this.loading = this.loadingCtrl.create({
            content: "Aguarde....",
        });
        this.alertCtrl = alertCtrl;

        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            let alert = this.alertCtrl.create({
                title: 'Sem conexão! :(',
                subTitle: 'O seu aparelho está sem acesso a internet. Ative a rede de dados ou conecte-se a uma rede Wi-Fi.',
                buttons: ["Ok"]
            });
            alert.present();
        });
    }

    ionViewDidLoad() {
        this.oneSignal.getIds().then(response => {
            this.nativeStorage.setItem('OneSignal', {
                pushToken: response.pushToken,
                userId: response.userId,
            }).then(
                () => {
                    this.login.pushToken = response.pushToken,
                        this.login.userId = response.userId
                },
                error => console.error('Error OneSignal item', error)
                );
        },
            error => {
                console.log(error);
            });
    }

    ionViewDidEnter() {
        this.nativeStorage.getItem('User')
            .then(
            data => {
                this.loading.present();
                this.app.getRootNav().push(HomePage);
                this.loading.dismiss();
            },
            error => {
                this.dialogs.alert("Sessão Expirada, Favor Realize seu Login ", 'Atenção', 'Ok');
            });
    }

    public doLogin() {
        this.loading.present();
        this.oneSignal.getIds().then(response => {
            this.nativeStorage.setItem('OneSignal', {
                pushToken: response.pushToken,
                userId: response.userId,
            }).then(
                () => {
                    console.log(response);
                    this.login.pushToken = response.pushToken,
                        this.login.userId = response.userId
                },
                error => console.error('Error OneSignal item', error)
                );
        },
            error => {
                console.log(error);
            });

        this.webservice.getLogin(this.login).subscribe(response => {
            if (response.status) {
                if (this.device.uuid != null) {
                    this.nativeStorage.setItem('User', {
                        uuid: this.device.uuid,
                        serial: this.device.serial,
                        cpf_cnpj: response.obj.cpf_cnpj,
                        nome: response.obj.nome,
                        login: response.obj.login,
                        tipo: response.obj.tipo_usuario,
                        token: response.obj.token,
                        condominio: response.obj.condominio,
                        nome_condominio: response.obj.nome_condominio,
                        logo_condominio: response.obj.logo_condominio,
                        bloco: response.obj.bloco,
                        id_apto: response.obj.id_apto,
                        num_apto: response.obj.num_apto
                    })
                        .then(
                        () => console.log('Stored User!'),
                        error => console.error('Error storing item', error)
                        );
                }

                this.navCtrl.setRoot(HomePage);
                this.loading.dismiss();
            } else {
                this.loading.dismiss();
                this.dialogs.alert(response.msg, 'Atenção', 'ok');
            }
        });
    }

    forgotPassword() {
        let alert = this.alertCtrl.create({
            title: ' Esqueceu a senha ?!',
            subTitle: ' Sua senha foi enviada para o seu e-mail de cadastro!',
            buttons: ['OK']
        });
        alert.present();
    }
}
