import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';

import { WebServiceProvider } from '../../providers/web-service';

import { TokenCondominio } from '../../interface/tokenCondominio';
import { UserData } from '../../interface/user/userData';

import { LoginPage } from "../login/login";
import { OrdersPage } from "../orders/orders";
import { ReservationPage } from "../reservation/reservation";
import { FeedsPage } from "../feeds/feeds";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public tipoUsuario: string;
  public tokenCondominio: TokenCondominio = {
    token: '',
    condominio: ''
  };
  public userData: UserData = {
    nome: '',
    bloco: '',
    num_apto: '',
    avatar: 'https://www.condofeeds.com.br/condofeeds/assets/images/user_padrao.jpg'
  };
  tabBar: any;
  constructor(public navCtrl: NavController,
    public webservice: WebServiceProvider,
    public app: App,
    public nativeStorage: NativeStorage
  ) { }

  ionViewDidEnter() {
    this.nativeStorage.getItem('User')
      .then(
      data => {
        this.userData.nome = data.nome;
        this.userData.bloco = data.bloco;
        this.userData.num_apto = data.num_apto;
        this.tipoUsuario = data.tipo;
      },
      error => console.log(error)
      );
  }
  exit() {
    if (this.nativeStorage.remove('User')) {
      this.app.getRootNav().setRoot(LoginPage);
    } else {
      console.log("Error")
    }
  }
  order() {
    this.app.getRootNav().push(OrdersPage);
  }
  ListFeeds() {
    this.app.getRootNav().push(FeedsPage);
  }
  Reservation() {
    this.app.getRootNav().push(ReservationPage);
  }
}


