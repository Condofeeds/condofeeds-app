import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { Dialogs } from '@ionic-native/dialogs';
import { NativeStorage } from '@ionic-native/native-storage';

import { TokenList } from '../../interface/reservation/tokenList';
import { List } from '../../interface/reservation/list';

import { DetailsReservationPage } from '../details-reservation/details-reservation'

import { WebServiceProvider } from '../../providers/web-service';

@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html'
})
export class ReservationPage {

  public loading: any;
  public getList: List[];
  public tokenList: TokenList = {
    token: "",
    cnpj_condominio: "",
    id_area: "null"
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public webservice: WebServiceProvider,
    public loadingCtrl: LoadingController,
    public dialogs: Dialogs,
    public nativeStorage: NativeStorage
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Aguarde....",
    });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.nativeStorage.getItem('User').then(response => {
      this.tokenList.token = response.token;
      this.tokenList.cnpj_condominio = response.condominio;

      this.webservice.getList(this.tokenList).subscribe(response => {
        if (response.status) {
          this.loading.dismiss();
          this.getList = response.obj;
        } else {
          this.loading.dismiss();
          this.dialogs.alert(response.msg, 'Atenção', 'Ok');
        }
      }, error => {
        this.loading.dismiss();
        this.dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
      });
    }, error => {
      this.loading.dismiss();
      this.dialogs.alert("Não possível buscar dados do usuário.", 'Atenção', 'Ok');
    });

  }
  DetailsReservation(details: List) {
    this.navCtrl.push(DetailsReservationPage, { details: details });
  }
}
