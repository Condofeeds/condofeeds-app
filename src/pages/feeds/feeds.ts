import { Component } from '@angular/core';
import { NavController, NavParams  } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { NativeStorage } from '@ionic-native/native-storage';

import { ListFeeds } from '../../interface/listFeeds';
import { TokenCondominio } from '../../interface/tokenCondominio';

import { DetailFeedsPage } from "../detail-feeds/detail-feeds";

import { WebServiceProvider } from '../../providers/web-service';
@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html'
})

export class FeedsPage {
  public listFeeds: ListFeeds[];
  public token: TokenCondominio = {
    token: "",
    condominio: ""
  }
  public refresher;
  public isRefreshing: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public webservice: WebServiceProvider,
    public nativeStorage: NativeStorage,
    public dialogs: Dialogs
  ) { }

  doRefresh(refresher) {
    this.refresher = refresher;
    this.isRefreshing = true;
    this.loadFeeds();
  }

  ionViewDidEnter() {
    this.loadFeeds();
  }

  loadFeeds() {
    this.nativeStorage.getItem('User').then(response => {
      this.token.token = response.token;
      this.token.condominio = response.condominio;

      this.webservice.getFeeds(this.token).subscribe(response => {
        if (response.status) {
          this.listFeeds = response.obj;
          if (this.isRefreshing) {
            this.refresher.complete();
            this.isRefreshing = false;
          }
        } else {
          this.dialogs.alert(response.msg, 'Atenção', 'Ok');
          if (this.isRefreshing) {
            this.refresher.complete();
            this.isRefreshing = false;
          }
        }
      }, error => {
        this.dialogs.alert("Não possível buscar Comunicados.", 'Atenção', 'Ok');
        if (this.isRefreshing) {
          this.refresher.complete();
          this.isRefreshing = false;
        }
      });
    }, error => {
      this.dialogs.alert("Não possível buscar dados do usuário.", 'Atenção', 'Ok');
      if (this.isRefreshing) {
        this.refresher.complete();
        this.isRefreshing = false;
      }
    });
  }

  openFeeds(details: ListFeeds) {
    this.navCtrl.push(DetailFeedsPage, { details: details });
  }
}
