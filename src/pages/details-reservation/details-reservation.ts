import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { TokenList } from '../../interface/reservation/tokenList';
import { TokenListEvent } from '../../interface/reservation/tokenListEvent';
import { DetailList } from '../../interface/reservation/detailList';
import { ListReservation } from '../../interface/reservation/listReservation';

import { NativeStorage } from '@ionic-native/native-storage';
import { Dialogs } from '@ionic-native/dialogs';

import { WebServiceProvider } from '../../providers/web-service';


@Component({
  selector: 'page-details-reservation',
  templateUrl: 'details-reservation.html'
})
export class DetailsReservationPage {
  eventList;
  public loading: any;  
  public detailList: DetailList = {
    nome: "",
    descricao: null,
    valor: "",
    limite_pessoas: "",
    limite_horario: "",
    id_area: ""
  };
  public getListEvent: ListReservation[];
  public tokenListEvent: TokenListEvent = {
    token: "",
    cnpj_condominio: "",
    id_area_comun: "",
    aprovacao: "A",
    week: "5"
  };

  constructor(  public navCtrl: NavController,
                public navParams: NavParams, 
                public modalCtrl: ModalController, 
                public webservice: WebServiceProvider, 
                public loadingCtrl: LoadingController,
                public nativeStorage: NativeStorage,
                public dialogs: Dialogs
              ){
        this.loading = this.loadingCtrl.create({
        content: `<ion-spinner name="crescent"></ion-spinner>`,
    });
  }
  ionViewDidLoad() {
    this.detailList = this.navParams.get('details');

  /*this.tokenListEvent.id_area_comun = this.detailList.id_area;
    this.webservice.getListEvent(this.tokenListEvent).then(response => {
      if (response.status) {
        this.getListEvent = response.obj;
      } else {
        this.eventList = true;
      }
    }, error => {
      console.log(error);
      Dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
    });  */

       this.nativeStorage.getItem('User').then(response => {
       this.tokenListEvent.token = response.token;
       this.tokenListEvent.cnpj_condominio = response.condominio;
       this.tokenListEvent.id_area_comun = this.detailList.id_area;

       this.webservice.getListEvent(this.tokenListEvent).subscribe(response => {
          if (response.status) {
             this.loading.present();
               setTimeout(() => {
                   this.loading.dismiss();
                   this.getListEvent = response.obj;

                   },300);
          } else {
            this.loading.present();
              setTimeout(() => {
                   this.loading.dismiss();
                   this.eventList = true;
                   },300);
          }
        }, error => {
          this.dialogs.alert("Não possível buscar Areas Comuns.", 'Atenção', 'Ok');
        });
      }, error => {
        alert('error');
      }); 
  }

  openModal() {
    let modal = this.modalCtrl.create(ModalPage);
    modal.present();
  }
}

@Component({
  template: `<ion-header>
  <ion-toolbar>
    <ion-title>
      Reserva
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <ion-icon name="md-close" ></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content>
<ion-card>
  <ion-item class="top">
    <ion-avatar item-left>
      <img src="img/marty-avatar.png">
    </ion-avatar>
    <h2>Welton Lima</h2>
    <p>Apartamento 1004 - Bloco 6 </p>
  </ion-item>
  <ion-item class="section-1">
     <p item-left>Selecione uma data para a reserva</p>
  </ion-item>    
    <ion-item > 
    <span item-left>
        <ion-icon ion-fab mini name="calendar"></ion-icon>
    </span>
     <span item-left>
        <ion-datetime cancelText="Cancelar" doneText="Aceitar" displayFormat="DD MMMM YYYY" min="{{min}}" max="{{year}}" ></ion-datetime>
    </span> 
  </ion-item>
      <ion-item class="footer">
        <button ion-button icon-left clear item-right>
         CONFIRMAR
    </button>
  </ion-item>
</ion-card>`
})

export class ModalPage {
  character;
  today
  year
  month
  day
  min
  constructor(public params: NavParams, public viewCtrl: ViewController) {
    this.day = new Date().getUTCDate();
    this.month = new Date().getMonth() + 1;
    this.year = new Date().getFullYear();
   
    if (this.month < 10) {
      this.month = "0" + this.month
    } if (this.day < 10){
      this.day = "0" + this.day
    }else {
      this.month = this.month
    }
  
    this.min = this.year + "-" + this.month + "-" + this.day;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}