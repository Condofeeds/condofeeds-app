import {
  Component,
  Input
} from '@angular/core';

/**
 * Generated class for the FeedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'feed',
  templateUrl: 'feed.html'
})
export class FeedComponent {
  @Input('endIcon') endIcon = 'ionic';
  constructor() {
  }
}
@Component({
  selector: 'feed-item',
  template: '<ng-content></ng-content>'
})
export class FeedItemComponent {
  
  constructor() {}
}

@Component({
  selector: 'feed-tittle',
  template: '<span>{{titulo}}</span>'
})
export class FeedTittleComponent {
  @Input('tittle') titulo?: string = '';
  constructor() {}
}
